﻿using System;


namespace ConsoleApp1
{
    internal class Program
    {
        private static int[] GetSecretNumber()
        {
            int[] secretNumber = new int[4] {-1,-1,-1,-1};
            for(int i =0; i<4; )
            {
                Random rnd = new Random();
                int num = rnd.Next(10);
                if (Array.IndexOf(secretNumber, num)!=-1)
                    continue;
                secretNumber[i] =num;
                i++;
            }
            for(int i = 0; i<secretNumber.Length; i++)
            {
                Console.Write(secretNumber[i]);
            }
            Console.WriteLine();

            return secretNumber;
        }

        private static int[] getGuessNumber()
        {
            int[] intguess = new int[4] {-1,-1,-1,-1};
            int i = 0;
            while (true)
            {
                if (i==4) return intguess;
                else for (int a=0;i<intguess.Length;i++) intguess[a]=-1;
                try
                {
                    i=0;
                    String guess = Console.ReadLine();
                    if (guess.Length !=4)
                    {
                        Console.WriteLine("Wrong input");
                        continue;
                    }
                    foreach (char a in guess)
                    {
                        if (Array.IndexOf(intguess, int.Parse(a.ToString()))!=-1)
                        {
                            Console.WriteLine("Wrong input");
                            break;
                        }
                            
                        intguess[i] = int.Parse(a.ToString());
                        i++;
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine("wrong input");
                }
            }
        }
        private static void guesses(int[] secretnumber)
        {
            int i = 0;
            string secret = "";
            foreach (int s in secretnumber)
                secret+=s;
            while (i<8)
            {   
                int p=0, m=0;
                int[] intguess= getGuessNumber();
                for(int a = 0; a<4; a++)
                {
                    if ((Array.IndexOf(secretnumber, intguess[a])!=-1) && (Array.IndexOf(secretnumber, intguess[a])==Array.IndexOf(intguess, intguess[a])))
                        p+=1;
                    if ((Array.IndexOf(secretnumber, intguess[a])!=-1) && !(Array.IndexOf(secretnumber, intguess[a])==Array.IndexOf(intguess, intguess[a])))
                        m+=1;

                }
                
                if (p==4){
                    Console.WriteLine("Guessed");
                    return;
                }
                Console.WriteLine($"M:{m} P:{p}");
                i++;
            }
            Console.WriteLine($"not guessed, the secret number was {secret}");
        }
        
        static void Main(string[] args)
        {
            int[] secretNumber = GetSecretNumber();
            guesses(secretNumber);

        }
    }
}
