﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Handler h1 = new Project_Manager();
            Handler h2 = new Program_Manager();
            Handler h3 = new Subdivision_Manager();
            Handler h4 = new Division_Director();
            Handler h5 = new CEO();
            h1.SetSuccessor(h2);
            h2.SetSuccessor(h3);
            h3.SetSuccessor(h4);
            h4.SetSuccessor(h5);
            try
            {
                while (true) {
                    int input = Convert.ToInt32(Console.ReadLine());
                    if (input <0) continue;
                    else h1.HandleRequest(input);
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }
            
            
            
        }
    }
    public abstract class Handler
    {
        protected Handler successor;
        public void SetSuccessor(Handler successor)
        {
            this.successor = successor;
        }
        public abstract void HandleRequest(int request);
    }

    public class Project_Manager : Handler
    {
        public override void HandleRequest(int cost)
        {
            if (cost > 0 && cost <= 500)
            {
                Console.WriteLine("{0} authorized to approve {1}$",
                    this.GetType().Name, cost);
            }
            else if (successor != null)
            {
                successor.HandleRequest(cost);
            }
        }
    }

    public class Program_Manager : Handler
    {
        public override void HandleRequest(int cost)
        {
            if (cost > 500 && cost <= 2000)
            {
                Console.WriteLine("{0} authorized to approve {1}$",
                    this.GetType().Name, cost);
            }
            else if (successor != null)
            {
                successor.HandleRequest(cost);
            }
        }
    }
    public class Subdivision_Manager : Handler
    {
        public override void HandleRequest(int cost)
        {
            if (cost > 2000 && cost <= 5000)
            {
                Console.WriteLine("{0} authorized to approve {1}$",
                    this.GetType().Name, cost);
            }
            else if (successor != null)
            {
                successor.HandleRequest(cost);
            }
        }
    }
    public class Division_Director : Handler
    {
        public override void HandleRequest(int cost)
        {
            if (cost > 5000 && cost <= 20000)
            {
                Console.WriteLine("{0} authorized to approve {1}$",
                    this.GetType().Name, cost);
            }
            else if (successor != null)
            {
                successor.HandleRequest(cost);
            }
        }
    }
    public class CEO : Handler
    {
        public override void HandleRequest(int cost)
        {
            if (cost > 20000 && cost <= 100000)
            {
                Console.WriteLine("{0} authorized to approve {1}$",
                    this.GetType().Name, cost);
            }
            else
            {
                Console.WriteLine("The cost is too big");
                
            }
        }
    }

}
